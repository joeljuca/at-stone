# Banking API

[![pipeline status](https://gitlab.com/joelwallis/at-stone/badges/master/pipeline.svg)](https://gitlab.com/joelwallis/at-stone/commits/master)
[![coverage report](https://gitlab.com/joelwallis/at-stone/badges/master/coverage.svg)](https://gitlab.com/joelwallis/at-stone/commits/master)

Technical challenge for [Stone](https://www.stone.com.br/).

See: https://gist.github.com/bamorim/39f7ec5ba2c5beff6ff0227a4e6308ca

## Up and Running

The following instructions will help you run Banking API as a Docker container. You need to have Docker properly installed on the targeted environment. Installation instructions can be found at [docs.docker.com/install](https://docs.docker.com/install/).

First, clone the repository:

```
git clone https://gitlab.com/joelwallis/at-stone.git
cd at-stone
```

Then, build the Docker image:

```
docker build -t joelwallis/at-stone .
```

Now, just run it as a normal Docker container:

```
docker run -it --rm -p 8080:8080 joelwallis/at-stone
```

Banking API will be available at [localhost:8080](http://localhost:8080/).
