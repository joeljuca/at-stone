FROM elixir:1.7.4-alpine

LABEL maintainer="Joel Wallis Jucá <joelwallis@gmail.com>"

ENV MIX_ENV prod

WORKDIR /app
COPY . $WORKDIR

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile

EXPOSE 8080

CMD mix run --no-halt
